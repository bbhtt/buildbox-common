/*
 * Copyright 2024 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_logstreamreader.h>

namespace buildboxcommon {

LogStreamReader::LogStreamReader(const std::string &resourceName,
                                 const ConnectionOptions &connectionOptions)
    : LogStreamReader(resourceName,
                      ByteStream::NewStub(connectionOptions.createChannel()),
                      std::stoi(connectionOptions.d_retryLimit),
                      std::stoi(connectionOptions.d_retryDelay))
{
}

LogStreamReader::LogStreamReader(
    const std::string &resourceName,
    std::shared_ptr<ByteStream::StubInterface> byteStreamClient,
    const int grpcRetryLimit, const int grpcRetryDelay)
    : d_resourceName(resourceName),
      d_grpcRetryLimit(static_cast<unsigned int>(grpcRetryLimit)),
      d_grpcRetryDelay(grpcRetryDelay), d_byteStreamClient(byteStreamClient)
{
}

bool LogStreamReader::read(std::function<void(const std::string &)> onMessage,
                           std::function<bool()> shouldStop)
{
    BUILDBOX_LOG_TRACE("Reading LogStream " << d_resourceName);
    size_t currentReadOffset = 0;

    auto readLambda = [&](grpc::ClientContext &context) {
        ReadRequest request;
        request.set_resource_name(d_resourceName);
        request.set_read_offset(currentReadOffset);
        auto reader = this->d_byteStreamClient->Read(&context, request);

        ReadResponse response;
        while (reader->Read(&response)) {
            onMessage(response.data());
            currentReadOffset += response.data().length();
            if (shouldStop()) {
                return grpc::Status::CANCELLED;
            }
        }

        const grpc::Status readStatus = reader->Finish();
        return readStatus;
    };

    GrpcRetrier retrier(d_grpcRetryLimit, d_grpcRetryDelay, readLambda,
                        "ByteStream.Read()");

    if (retrier.issueRequest() && retrier.status().ok()) {
        return true;
    }
    return false;
}

bool LogStreamReader::read()
{
    auto handler = [&](const std::string &data) { std::cout << data; };
    auto shouldStop = [&]() { return false; };
    return read(handler, shouldStop);
}

} // namespace buildboxcommon
