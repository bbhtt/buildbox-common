include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

add_executable(run_hosttools_tests
  hosttoolsrunner.t.cpp
  ../buildboxrun_hosttools.cpp
  ../buildboxrun_hosttools_changedirectoryguard.cpp
  ../buildboxrun_hosttools_pathprefixutils.cpp)
target_precompile_headers(run_hosttools_tests REUSE_FROM common)
target_include_directories(run_hosttools_tests PRIVATE "..")
target_link_libraries(run_hosttools_tests common ${GTEST_MAIN_TARGET} ${GTEST_TARGET})
add_test(NAME run_hosttools_tests COMMAND run_hosttools_tests)

add_executable(run_hosttools_pathprefixutils_tests
  pathprefixutils.t.cpp
  ../buildboxrun_hosttools_pathprefixutils.cpp)
target_precompile_headers(run_hosttools_pathprefixutils_tests REUSE_FROM common)
target_include_directories(run_hosttools_pathprefixutils_tests PRIVATE "..")
target_link_libraries(run_hosttools_pathprefixutils_tests common ${GTEST_MAIN_TARGET} ${GTEST_TARGET})
add_test(NAME run_hosttools_pathprefixutils_tests COMMAND run_hosttools_pathprefixutils_tests)

add_executable(run_hosttools_changedirectoryguard_tests
  changedirectoryguard.t.cpp
  ../buildboxrun_hosttools_changedirectoryguard.cpp)
target_precompile_headers(run_hosttools_changedirectoryguard_tests REUSE_FROM common)
target_include_directories(run_hosttools_changedirectoryguard_tests PRIVATE "..")
target_link_libraries(run_hosttools_changedirectoryguard_tests common ${GTEST_MAIN_TARGET} ${GTEST_TARGET})
add_test(NAME run_hosttools_changedirectoryguard_tests COMMAND run_hosttools_changedirectoryguard_tests)
