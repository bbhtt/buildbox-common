set(SRCS
    buildboxworker_actionutils.cpp
    buildboxworker_botsessionutils.cpp
    buildboxworker_cmdlinespec.cpp
    buildboxworker_config.cpp
    buildboxworker_logstreamdebugutils.cpp
    buildboxworker_executeactionutils.cpp
    buildboxworker_expiretime.cpp
    buildboxworker_metricnames.cpp
    buildboxworker_worker.cpp
)

add_library(buildboxworker STATIC ${SRCS})
target_precompile_headers(buildboxworker REUSE_FROM common)
target_include_directories(buildboxworker PRIVATE ".")
target_link_libraries(buildboxworker common commonmetrics)

add_executable(buildbox-worker
    buildboxworker.m.cpp
)
target_precompile_headers(buildbox-worker REUSE_FROM common)

if(CMAKE_BUILD_TYPE STREQUAL "DEBUG" AND LOGSTREAM_DEBUG)
    target_compile_definitions(buildboxworker PUBLIC LOGSTREAM_DEBUG=1)
endif()

target_include_directories(buildbox-worker PRIVATE ".")

target_link_libraries(buildbox-worker buildboxworker)

install(TARGETS buildbox-worker RUNTIME DESTINATION bin)

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
