#!/bin/bash

. $(dirname "$0")/fuse.sh

if ! [ -x "$(command -v getfattr)" ]; then
	echo "getfattr is required for xattr tests" >&2
	exit 1
fi

# Create the base via FUSE
start_buildbox
echo "lorem ipsum" > "$ROOT"/file

restart_buildbox

# Calculate SHA256 hash and format it the same way as the output of getfattr
sha256sum "$ROOT"/file | sed -e 's/^\([^ ]\+\).*$/user.checksum.sha256="\1"\n/' > "$TMPDIR"/expected

# Get SHA256 hash from extended attributes
getfattr --name=user.checksum.sha256 "$ROOT"/file | grep -v '^#' > "$TMPDIR"/actual

if ! diff -u "$TMPDIR"/expected "$TMPDIR"/actual ; then
	echo "Extended attribute test failure for unmodified regular file" >&2
	exit 1
fi

# Modify file
echo "dolor" >> "$ROOT"/file
# Verify that no hash is returned for modified files
if getfattr --name=user.checksum.sha256 "$ROOT"/file ; then
	echo "Unexpected extended attribute for modified regular file" >&2
	exit 1
fi

stop_buildbox
