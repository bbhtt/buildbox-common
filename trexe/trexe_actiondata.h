/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_TREXE_ACTIONDATA
#define INCLUDED_TREXE_ACTIONDATA

#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>
#include <memory>

/*
 * POD type containing input data for use in a remotely executed Action.
 */

namespace trexe {

struct ActionData {
  public:
    std::shared_ptr<const buildboxcommon::Command> d_commandProto;
    std::shared_ptr<const buildboxcommon::Digest> d_commandDigest;
    std::shared_ptr<const buildboxcommon::Action> d_actionProto;
    std::shared_ptr<const buildboxcommon::Digest> d_actionDigest;
    std::shared_ptr<const buildboxcommon::digest_string_map>
        d_inputDigestsToPaths;
    std::shared_ptr<const buildboxcommon::digest_string_map>
        d_inputDigestsToSerializedProtos;
};

} // namespace trexe

#endif
