#!/bin/bash

mkdir -p /tmp/upload/input1;

echo "#!/bin/bash" > /tmp/upload/input1/hello.sh;
echo "echo \"hello\"" >> /tmp/upload/input1/hello.sh;

chmod +x /tmp/upload/input1/hello.sh;

/usr/local/bin/trexe --remote=http://controller:50051 --input-path=/tmp/upload/input1 --output-path=/home "./hello.sh";
