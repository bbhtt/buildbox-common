include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

# Every directory containing a source file needed by the tests, must be added here.
include_directories(. ..)

macro(add_trexe_test TEST_NAME TEST_SOURCE)
    # Create a separate test executable per test source.
    add_executable(trexe_${TEST_NAME} ${TEST_SOURCE})
    target_precompile_headers(trexe_${TEST_NAME} REUSE_FROM common)

    # This allows us to pass an optional argument if the cwd for the test is not the default.
    set(ExtraMacroArgs ${ARGN})
    list(LENGTH ExtraMacroArgs NumExtraArgs)
    if(${NumExtraArgs} GREATER 0)
      list(GET ExtraMacroArgs 0 TEST_WORKING_DIRECTORY)
    else()
      set(TEST_WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    endif()

    add_test(NAME trexe_${TEST_NAME} COMMAND trexe_${TEST_NAME} WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY})
    target_link_libraries(trexe_${TEST_NAME} PUBLIC trexe-lib ${GMOCK_TARGET} ${GTEST_MAIN_TARGET})
endmacro()

add_trexe_test(actionbuilder_tests trexe_actionbuilder.t.cpp)
add_trexe_test(ninja_cmdlinespec_tests trexe_cmdlinespec.t.cpp)
add_trexe_test(executionoptions_tests trexe_executionoptions.t.cpp)
add_trexe_test(executioncontext_tests trexe_executioncontext.t.cpp)
