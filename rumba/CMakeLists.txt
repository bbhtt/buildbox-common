# define RUMBA_CONFIG_DIR
add_definitions(-DRUMBA_CONFIG_DIR="${RUMBA_CONFIG_DIR}")

add_subdirectory(rumbad)

add_executable(rumba
    rumba.m.cpp
    rumba_datautils.cpp
)

target_include_directories(rumba PUBLIC . ..)
target_precompile_headers(rumba REUSE_FROM common)
target_link_libraries(rumba PUBLIC remoteexecution)

install(
    TARGETS rumba
    DESTINATION bin
    COMPONENT rumba
)

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
