// Copyright 2024 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <rumbad_compositewriter.h>
#include <rumbad_writer.h>

namespace rumbad {

CompositeWriter::CompositeWriter() {}

CompositeWriter::CompositeWriter(
    std::vector<std::unique_ptr<Writer>> writerList)
    : d_vectorOfWriters(std::move(writerList))
{
}

void CompositeWriter::addWriter(std::unique_ptr<Writer> writer)
{
    d_vectorOfWriters.push_back(std::move(writer));
}

void CompositeWriter::write(const std::vector<Message> &messages)
{
    for (std::unique_ptr<Writer> &writer : d_vectorOfWriters) {
        writer.get()->write(messages);
    }
}

} // namespace rumbad
