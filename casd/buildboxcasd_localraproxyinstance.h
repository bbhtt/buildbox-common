/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALRAPROXYINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALRAPROXYINSTANCE_H

#include <buildboxcasd_fslocalassetstorage.h>
#include <buildboxcasd_rainstance.h>
#include <buildboxcommon_assetclient.h>
#include <buildboxcommon_connectionoptions.h>

#include <memory>

using namespace build::bazel::remote::execution::v2;

using grpc::Status;

namespace buildboxcasd {

class LocalRaProxyInstance final : public RaInstance {
    /* This class implements the logic for methods that service a RA server
     * that also acts as a proxy pointing to a remote one.
     */
  public:
    LocalRaProxyInstance(
        std::shared_ptr<FsLocalAssetStorage> asset_storage,
        const buildboxcommon::ConnectionOptions &asset_endpoint);

    grpc::Status FetchBlob(const FetchBlobRequest &request,
                           FetchBlobResponse *response) override;

    grpc::Status FetchDirectory(const FetchDirectoryRequest &request,
                                FetchDirectoryResponse *response) override;

    grpc::Status PushBlob(const PushBlobRequest &request,
                          PushBlobResponse *response) override;

    grpc::Status PushDirectory(const PushDirectoryRequest &request,
                               PushDirectoryResponse *response) override;

  private:
    std::shared_ptr<FsLocalAssetStorage> d_assetStorage;
    std::unique_ptr<buildboxcommon::AssetClient> d_assetClient;
    std::string d_remoteInstanceName;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALRAPROXYINSTANCE_H
