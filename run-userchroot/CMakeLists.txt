add_executable(buildbox-run-userchroot
    buildboxrun_userchroot.cpp
    buildboxrun_chrootmanager.cpp
    buildboxrun_commandfilemanager.cpp
    buildboxrun_userchroot.m.cpp
)
target_precompile_headers(buildbox-run-userchroot REUSE_FROM common)

install(TARGETS buildbox-run-userchroot RUNTIME DESTINATION bin)
target_link_libraries(buildbox-run-userchroot common)
target_include_directories(buildbox-run-userchroot PRIVATE ".")

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
